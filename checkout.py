"""
Backend Exercise in Python
@ Agustin Jofre Millet
"""

import pandas as pd


""" Products sold by the store """
products = {'VOUCHER': ['Gift Card', 5.00],
           'TSHIRT': ['Summer T-Shirt', 20.00],
           'PANTS': ['Summer Pants', 7.50]}

""" Class that calculates price rules from a list """
class Pricing_rules:
	def calculate_price_rules (shop_list): #. calculates the total taking into account each price rule
		total = 0  # variable with discounted total

		shop_list.columns = ['Name', 'Price']
		shop_list = shop_list.groupby('Name').agg({'Price': 'sum','Name':'count'})

		for i, j, in shop_list.iterrows():
			if (i == 'Gift Card'): # 2-for-1 special on VOUCHER
				if (j['Name'] % 2 == 0):
					total += j['Price'] / 2
				else:
					total += (j['Price'] - products['VOUCHER'][1]) / 2 + products['VOUCHER'][1]

			elif (i == 'Summer T-Shirt'): # If you buy 3 or more TSHIRT items, the price per unit should be 19.00€
				if (j['Name'] >= 3):
					total += j['Name'] * (products['TSHIRT'][1] - 1)
				else:
					total += j['Price']
			elif (i == 'Summer Pants'):
				total += j['Price']

		return total

""" Class that is responsible for creating an empty list, scanning products and calculating the total with price rule """
class Checkout (Pricing_rules):

    def __init__(self):
        self.shop_list = []    # creates a new empty list for each shopping list

    def scan_prod(self, prod):  # function that scans a product
        self.shop_list.append(prod)

    def total_witout_discount (self):  # total price of the items WITHOUT Price Rules
    	total_witout = 0
    	for i in self.shop_list:
    		total_witout += i[1]
    	return total_witout

    def total_price (self):   # total price of the items WITH Price Rules
    	return Pricing_rules.calculate_price_rules (pd.DataFrame(self.shop_list))


""" Test Examples """
# test1: Items: VOUCHER, TSHIRT, PANTS - Total: 32.50€
t1 = Checkout()
t1.scan_prod(products['VOUCHER'])
t1.scan_prod(products['TSHIRT'])
t1.scan_prod(products['PANTS'])

print('Total : ', format(t1.total_witout_discount(),'.2f'), '€')
print('Total to pay with discount: ', format(t1.total_price(),'.2f'), '€\n')

# test 2: Items: VOUCHER, TSHIRT, VOUCHER - Total: 25.00€
t2 = Checkout()
t2.scan_prod(products['VOUCHER'])
t2.scan_prod(products['TSHIRT'])
t2.scan_prod(products['VOUCHER'])

print('Total : ', format(t2.total_witout_discount(),'.2f'), '€')
print('Total to pay with discount: ', format(t2.total_price(),'.2f'), '€\n')

# test 3: Items: TSHIRT, TSHIRT, TSHIRT, VOUCHER, TSHIRT - Total: 81.00€
t3 = Checkout()
t3.scan_prod(products['TSHIRT'])
t3.scan_prod(products['TSHIRT'])
t3.scan_prod(products['TSHIRT'])
t3.scan_prod(products['VOUCHER'])
t3.scan_prod(products['TSHIRT'])

print('Total : ', format(t3.total_witout_discount(),'.2f'), '€')
print('Total to pay with discount: ', format(t3.total_price(),'.2f'), '€\n')

# test 4: Items: VOUCHER, TSHIRT, VOUCHER, VOUCHER, PANTS, TSHIRT, TSHIRT - Total: 74.50€
t4 = Checkout()
t4.scan_prod(products['VOUCHER'])
t4.scan_prod(products['TSHIRT'])
t4.scan_prod(products['VOUCHER'])
t4.scan_prod(products['VOUCHER'])
t4.scan_prod(products['PANTS'])
t4.scan_prod(products['TSHIRT'])
t4.scan_prod(products['TSHIRT'])

print('Total : ', format(t4.total_witout_discount(),'.2f'), '€')
print('Total to pay with discount: ', format(t4.total_price(),'.2f'), '€\n')